let
  nixpkgs = builtins.fetchTarball {
    name = "nixpkgs-unstable-intracdc";
    url = "https://github.com/NixOS/nixpkgs/archive/3327b9a385f5ec2e219bf93b48896772b3f66afc.tar.gz";
    sha256 = "1kmrxz809z71rbi0vwslqxzpd22skfd5qilgf1lpnggrh11l5q6v";
  };
in { pkgs ? import nixpkgs {} }:

with pkgs;

let
  pythonEnv = poetry2nix.mkPoetryEnv {
    projectDir = ./.;
    python = python38;
    overrides = poetry2nix.overrides.withDefaults (
      self: super: {
        python-jose = super.python-jose.overridePythonAttrs(old: {
          postPatch = ''
            substituteInPlace setup.py --replace "'pytest-runner'," ""
            substituteInPlace setup.py --replace "'pytest-runner'" ""
          '';
        });
        social-auth-backend-epita = super.social-auth-backend-epita.overridePythonAttrs(old: {
          postPatch = ''
            substituteInPlace setup.py --replace "'social-auth-core[openidconnect]'," ""
          '';
        });
      }
    );
  };
in pkgs.mkShell {
  buildInputs = with pkgs; [
    python38
    pythonEnv
    poetry
    postgresql
    docker-compose
  ];
}
