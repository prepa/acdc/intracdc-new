from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    PermissionRequiredMixin,
)
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404, render
from django.views.generic import TemplateView

from .forms import PreferencesForm
from .utils import order_dict


class ProfileView(PermissionRequiredMixin, TemplateView):
    permission_required = "accounts.can_view_user"
    template_name = "accounts/profile.html"

    def get(self, request, *args, **kwargs):
        context = {
            "user": get_object_or_404(
                get_user_model(), username=kwargs["username"]
            ),
        }
        return render(request, self.template_name, context)


class PreferencesView(LoginRequiredMixin, TemplateView):
    template_name = "accounts/preferences.html"

    def get(self, request, *args, **kwargs):
        context = {"form": PreferencesForm(instance=request.user)}
        return render(request, self.template_name, context)

    # pylint:disable=unused-argument
    def post(self, request, *args, **kwargs):
        form = PreferencesForm(request.POST, instance=request.user)
        if form.is_valid():
            edited_user = form.save()
            edited_user.save()
        context = {
            "form": form,
        }
        return render(request, self.template_name, context)


class TrombiView(LoginRequiredMixin, TemplateView):
    template_name = "accounts/trombi.html"

    def get(self, request, *args, **kwargs):
        promotions_by_group = {}
        for group in Group.objects.all():
            if group.name == "CRI":
                continue
            promotions = {}

            for user in group.user_set.all():
                if user.current_class is None:
                    continue
                if user.current_class.semester.promotion not in promotions:
                    promotions[user.current_class.semester.promotion] = {}
                if (
                    user.current_class.name
                    not in promotions[user.current_class.semester.promotion]
                ):
                    promotions[user.current_class.semester.promotion][
                        user.current_class.name
                    ] = []
                promotions[user.current_class.semester.promotion][
                    user.current_class.name
                ].append(user)

            promotions_by_group[group.name] = promotions

        context = {
            "promotions_by_group": order_dict(promotions_by_group),
        }
        return render(request, self.template_name, context)
