from rest_framework.routers import DefaultRouter

from django.urls import include, path

# pylint:disable=unused-import
from . import app_name  # noqa: F401
from .api_views import (
    CampusViewSet,
    SemesterViewSet,
    ClassViewSet,
    GroupViewSet,
    AccountViewSet,
)

router = DefaultRouter()
router.register(r"campuses", CampusViewSet)
router.register(r"classes", ClassViewSet)
router.register(r"semesters", SemesterViewSet)
router.register(r"users", AccountViewSet)
router.register(r"groups", GroupViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
