from rest_framework import serializers

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from themes import app_name as themes_app_name
from themes.models import Theme

from .models import Campus, Semester, Class
from . import app_name


class CampusSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Campus
        fields = ("url", "id", "name")
        extra_kwargs = {"url": {"view_name": f"{app_name}:campus-detail"}}


class SemesterSerializer(serializers.HyperlinkedModelSerializer):
    display_name = serializers.SerializerMethodField()

    class Meta:
        model = Semester
        fields = (
            "url",
            "id",
            "number",
            "is_sharp",
            "promotion",
            "display_name",
        )
        read_only_fields = ("url", "id", "display_name")
        extra_kwargs = {"url": {"view_name": f"{app_name}:semester-detail"}}

    # pylint: disable=no-self-use
    def get_display_name(self, obj):
        return str(obj)


class ClassSerializer(serializers.HyperlinkedModelSerializer):
    campus = serializers.HyperlinkedRelatedField(
        queryset=Campus.objects.all(), view_name=f"{app_name}:campus-detail"
    )
    semester = serializers.HyperlinkedRelatedField(
        queryset=Semester.objects.all(),
        view_name=f"{app_name}:semester-detail",
    )

    display_name = serializers.SerializerMethodField()

    class Meta:
        model = Class
        fields = ("url", "id", "name", "campus", "semester", "display_name")
        extra_kwargs = {"url": {"view_name": f"{app_name}:class-detail"}}

    # pylint: disable=no-self-use
    def get_display_name(self, obj):
        return str(obj)


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ("url", "id", "name")
        extra_kwargs = {"url": {"view_name": f"{app_name}:group-detail"}}


class AccountSerializer(serializers.HyperlinkedModelSerializer):
    groups = serializers.HyperlinkedRelatedField(
        many=True,
        queryset=Group.objects.all(),
        view_name=f"{app_name}:group-detail",
    )
    current_class = serializers.HyperlinkedRelatedField(
        queryset=Class.objects.all(), view_name=f"{app_name}:class-detail",
    )
    past_classes = serializers.HyperlinkedRelatedField(
        many=True,
        queryset=Class.objects.all(),
        view_name=f"{app_name}:class-detail",
    )
    theme = serializers.HyperlinkedRelatedField(
        queryset=Theme.objects.all(),
        view_name=f"{themes_app_name}:theme-detail",
    )

    profile_picture = serializers.SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = (
            "url",
            "id",
            "username",
            "first_name",
            "last_name",
            "email",
            "pseudo",
            "profile_picture",
            "theme",
            "groups",
            "current_class",
            "past_classes",
        )
        read_only_fields = (
            "url",
            "id",
            "username",
            "groups",
            "current_class",
            "past_classes",
            "profile_picture",
            "email",
        )
        extra_kwargs = {"url": {"view_name": f"{app_name}:account-detail"}}

    # pylint: disable=no-self-use
    def get_profile_picture(self, obj):
        return f"https://photos.cri.epita.fr/thumb/{obj.username}"
