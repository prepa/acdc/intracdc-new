from django import forms
from django.contrib.auth import get_user_model


class PreferencesForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = [
            "pseudo",
            "theme",
        ]
