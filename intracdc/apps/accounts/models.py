from django.contrib.auth.models import AbstractUser
from django.db import models


class Campus(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Campuses"

    def __str__(self):
        return f"{self.name}"


class Semester(models.Model):
    number = models.SmallIntegerField()
    is_sharp = models.BooleanField(default=False)
    promotion = models.SmallIntegerField()

    def __str__(self):
        return f"{self.promotion} S{self.number}{'#' if self.is_sharp else ''}"


class Class(models.Model):
    name = models.CharField(max_length=255)
    campus = models.ForeignKey("Campus", on_delete=models.PROTECT)
    semester = models.ForeignKey("Semester", on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = "Classes"

    def __str__(self):
        return f"{self.campus} - {self.semester} - {self.name}"


class Account(AbstractUser):
    pseudo = models.CharField(max_length=255, blank=True)
    theme = models.ForeignKey(
        "themes.Theme",
        on_delete=models.PROTECT,
        limit_choices_to={"datedtheme": None},
    )
    current_class = models.ForeignKey(
        "Class", on_delete=models.PROTECT, null=True, blank=True
    )
    past_classes = models.ManyToManyField(
        "Class", related_name="past_classes", blank=True
    )

    def is_student(self):
        return self.groups.filter(name="Students").exists()

    def __str__(self):
        return f"{self.username}"

    def __lt__(self, other):
        return self.username < other.username
