from rest_framework import permissions


class AccountPermission(permissions.BasePermission):
    # pylint: disable=arguments-differ
    def has_object_permission(self, request, view, account):
        return (
            request.method in permissions.SAFE_METHODS
            or request.user.has_perm("accounts.edit_account")
            or request.user == account
        )
