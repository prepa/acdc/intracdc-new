from rest_framework import permissions
from rest_framework import viewsets

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from .models import Campus, Semester, Class
from .permissions import AccountPermission
from .serializers import (
    CampusSerializer,
    SemesterSerializer,
    ClassSerializer,
    GroupSerializer,
    AccountSerializer,
)


class CampusViewSet(viewsets.ModelViewSet):
    queryset = Campus.objects.all()
    serializer_class = CampusSerializer
    permission_classes = [permissions.DjangoModelPermissions]


class SemesterViewSet(viewsets.ModelViewSet):
    queryset = Semester.objects.all()
    serializer_class = SemesterSerializer
    permission_classes = [permissions.DjangoModelPermissions]


class ClassViewSet(viewsets.ModelViewSet):
    queryset = Class.objects.all()
    serializer_class = ClassSerializer
    permission_classes = [permissions.DjangoModelPermissions]


class AccountViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = AccountSerializer
    permission_classes = [
        permissions.DjangoModelPermissions,
        AccountPermission,
    ]


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.DjangoModelPermissions]
