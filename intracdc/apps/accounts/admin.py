from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy

from .models import Campus, Semester, Class, Account


@admin.register(Campus)
class CampusAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Semester)
class SemesterAdmin(admin.ModelAdmin):
    list_display = (
        "number",
        "is_sharp",
        "promotion",
    )

    list_fitler = (
        "number",
        "is_sharp",
        "promotion",
    )


@admin.register(Class)
class ClassAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "campus",
        "semester",
    )

    list_filter = (
        "name",
        "campus",
        "semester",
    )


@admin.register(Account)
class AccountAdmin(UserAdmin):
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "pseudo",
        "current_class",
        "theme",
    )

    list_filter = (
        "current_class",
        "past_classes",
        "groups",
        "is_superuser",
        "is_staff",
        "is_active",
        "theme",
    )

    # pylint:disable=line-too-long
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            gettext_lazy("Personal info"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "pseudo",
                    "theme",
                )
            },
        ),
        (
            gettext_lazy("Permissions"),
            {"fields": ("is_active", "is_staff", "is_superuser")},
        ),
        (
            gettext_lazy("Groups"),
            {
                "fields": (
                    "current_class",
                    "past_classes",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        (
            gettext_lazy("Important dates"),
            {"fields": ("last_login", "date_joined")},
        ),
    )
