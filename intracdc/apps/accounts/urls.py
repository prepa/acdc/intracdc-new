from django.urls import path
from .views import ProfileView, PreferencesView, TrombiView

app_name = "accounts"

urlpatterns = [
    path("profile/<str:username>", ProfileView.as_view(), name="profile"),
    path("preferences", PreferencesView.as_view(), name="preferences"),
    path("trombi", TrombiView.as_view(), name="trombi"),
]
