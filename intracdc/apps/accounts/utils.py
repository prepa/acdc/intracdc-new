def order_dict(dictionary):
    return {
        k: order_dict(v)
        if isinstance(v, dict)
        else (sorted(v) if isinstance(v, list) else v)
        for k, v in sorted(dictionary.items())
    }
