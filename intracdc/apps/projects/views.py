from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic import TemplateView

from .forms import ProjectEditForm
from .models import Project


class ProjectListView(LoginRequiredMixin, TemplateView):
    template_name = "projects/project-list.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})


class ProjectView(LoginRequiredMixin, TemplateView):
    template_name = "projects/project.html"

    def get(self, request, *args, **kwargs):
        project = get_object_or_404(Project, uuid=kwargs["project_uuid"])
        context = {
            "project": project,
        }

        return render(request, self.template_name, context)


class ProjectEditView(LoginRequiredMixin, TemplateView):
    template_name = "projects/project-edit.html"

    def get(self, request, *args, **kwargs):
        project = get_object_or_404(Project, uuid=kwargs["project_uuid"])

        if (
            not request.user.is_superuser
            and request.user not in project.managers.all()
        ):
            raise PermissionDenied

        context = {
            "project": project,
            "form": ProjectEditForm(instance=project),
        }
        return render(request, self.template_name, context)

    # pylint:disable=unused-argument
    def post(self, request, *args, **kwargs):
        project = get_object_or_404(Project, uuid=kwargs["project_uuid"])

        if (
            not request.user.is_superuser
            and request.user not in project.managers.all()
        ):
            raise PermissionDenied

        form = ProjectEditForm(request.POST, instance=project)
        if form.is_valid():
            edited_project = form.save()
            edited_project.save()
            return redirect("projects:project", project_uuid=project.uuid)
        context = {
            "project": project,
            "form": form,
        }
        return render(request, self.template_name, context)
