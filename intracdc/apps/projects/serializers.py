from markdownx.utils import markdownify
from rest_framework import serializers

from django.contrib.auth import get_user_model

from accounts.models import Semester
from accounts import app_name as accounts_app_name

from .models import Project, ProjectFile
from . import app_name


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    semester = serializers.HyperlinkedRelatedField(
        queryset=Semester.objects.all(),
        view_name=f"{accounts_app_name}:semester-detail",
    )
    managers = serializers.HyperlinkedRelatedField(
        many=True,
        queryset=get_user_model().objects.all(),
        view_name=f"{accounts_app_name}:account-detail",
    )
    rendered_description = serializers.SerializerMethodField()
    git_url = serializers.SerializerMethodField()
    type_display = serializers.SerializerMethodField()

    class Meta:
        model = Project
        fields = (
            "url",
            "uuid",
            "title",
            "number",
            "type",
            "semester",
            "managers",
            "description",
            "begin",
            "end",
            "rendered_description",
            "git_url",
            "type_display",
        )
        read_only_fields = (
            "url",
            "uuid",
            "number",
            "type",
            "semester",
            "managers",
            "begin",
            "end",
            "rendered_description",
            "git_url",
            "type_display",
        )
        extra_kwargs = {"url": {"view_name": f"{app_name}:project-detail"}}

    def get_git_url(self, obj):
        return obj.compute_git_url(self.context["request"].user)

    # pylint: disable=no-self-use
    def get_type_display(self, obj):
        return obj.get_type_display()

    # pylint: disable=no-self-use
    def get_rendered_description(self, obj):
        return markdownify(obj.description)


class ProjectFileSerializer(serializers.HyperlinkedModelSerializer):
    project = serializers.HyperlinkedRelatedField(
        queryset=Project.objects.all(), view_name=f"{app_name}:project-detail"
    )
    uploaded_by = serializers.HyperlinkedRelatedField(
        queryset=get_user_model().objects.all(),
        view_name=f"{accounts_app_name}:account-detail",
    )
    version = serializers.SerializerMethodField()
    type_display = serializers.SerializerMethodField()

    class Meta:
        model = ProjectFile
        fields = (
            "url",
            "uuid",
            "project",
            "file",
            "type",
            "type_display",
            "type_other_description",
            "is_private",
            "version",
            "uploaded_by",
            "uploaded_at",
        )
        read_only_fields = (
            "url",
            "uuid",
            "version",
            "type_display",
            "uploaded_by",
            "uploaded_at",
        )
        extra_kwargs = {"url": {"view_name": f"{app_name}:projectfile-detail"}}

    # pylint: disable=no-self-use
    def get_version(self, project_file):
        return project_file.get_version()

    # pylint: disable=no-self-use
    def get_type_display(self, project_file):
        return project_file.get_type_display()
