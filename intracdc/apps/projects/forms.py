from django import forms

from .models import Project


class ProjectEditForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [
            "title",
            "description",
        ]
