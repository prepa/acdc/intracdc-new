from rest_framework import permissions

from .models import Project


class ProjectPermission(permissions.BasePermission):
    # pylint: disable=arguments-differ
    def has_object_permission(self, request, view, project):
        user_projects = Project.objects.by_user(request.user)

        if request.method in permissions.SAFE_METHODS:
            return project in user_projects

        if request.user.is_student():
            return False

        return (
            request.user.is_superuser or request.user in project.managers.all()
        )


class ProjectFilePermission(ProjectPermission):
    # pylint: disable=arguments-differ
    def has_object_permission(self, request, view, project_file):
        project = project_file.project
        user_projects = Project.objects.by_user(request.user)

        if request.method in permissions.SAFE_METHODS:
            if request.user.is_student():
                return not project_file.is_private and project in user_projects
            return project in user_projects

        if request.user.is_student():
            return False

        return (
            request.user.is_superuser or request.user in project.managers.all()
        )
