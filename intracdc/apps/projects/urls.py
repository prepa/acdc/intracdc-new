from django.urls import path
from .views import ProjectListView, ProjectView, ProjectEditView

app_name = "projects"

urlpatterns = [
    path("", ProjectListView.as_view(), name="project-list"),
    path("<uuid:project_uuid>/", ProjectView.as_view(), name="project"),
    path(
        "<uuid:project_uuid>/edit/",
        ProjectEditView.as_view(),
        name="project-edit",
    ),
]
