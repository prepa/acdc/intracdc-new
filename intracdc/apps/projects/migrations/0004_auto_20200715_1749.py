# Generated by Django 3.0.8 on 2020-07-15 15:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0003_auto_20200715_1133"),
    ]

    operations = [
        migrations.AlterField(
            model_name="projectfile",
            name="type",
            field=models.CharField(
                choices=[
                    ("FR", "French subject"),
                    ("EN", "English subject"),
                    ("SK", "Skeleton"),
                    ("CR", "Correction"),
                    ("OT", "Other"),
                ],
                max_length=2,
            ),
        ),
    ]
