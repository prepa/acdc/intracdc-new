from rest_framework import generics
from rest_framework import permissions
from rest_framework import viewsets

from django.shortcuts import get_object_or_404

from .models import Project, ProjectFile
from .permissions import ProjectPermission, ProjectFilePermission
from .serializers import (
    ProjectSerializer,
    ProjectFileSerializer,
)


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.none()
    serializer_class = ProjectSerializer
    permission_classes = [
        permissions.DjangoModelPermissions,
        ProjectPermission,
    ]

    def get_queryset(self):
        return Project.objects.by_user(self.request.user)


class ProjectFileViewSet(viewsets.ModelViewSet):
    queryset = ProjectFile.objects.none()
    serializer_class = ProjectFileSerializer
    permission_classes = [
        permissions.DjangoModelPermissions,
        ProjectFilePermission,
    ]

    def get_queryset(self):
        return ProjectFile.objects.by_user(self.request.user)


class ProjectFileByProjectView(generics.ListAPIView):
    queryset = Project.objects.all()
    real_queryset = ProjectFile.objects.all()
    serializer_class = ProjectFileSerializer
    permission_classes = [
        permissions.DjangoModelPermissions,
        ProjectFilePermission,
    ]
    lookup_field = "uuid"
    lookup_url_kwarg = "uuid"

    def get_queryset(self):
        if not self.kwargs:
            return Project.objects.by_user(self.request.user)
        project = get_object_or_404(
            Project, uuid=self.kwargs[self.lookup_url_kwarg]
        )
        return ProjectFile.objects.by_project(project)
