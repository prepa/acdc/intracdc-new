from markdownx.admin import MarkdownxModelAdmin

from django.contrib import admin

from .models import Project, ProjectFile


@admin.register(Project)
class ProjectAdmin(MarkdownxModelAdmin):
    list_display = (
        "title",
        "number",
        "semester",
        "begin",
        "end",
        "uuid",
    )

    list_filter = (
        "semester",
        "managers",
    )


@admin.register(ProjectFile)
class ProjectFileAdmin(admin.ModelAdmin):
    list_display = (
        "file",
        "project",
        "type",
        "is_private",
        "uploaded_by",
        "uploaded_at",
    )

    list_filter = (
        "project",
        "type",
        "is_private",
        "uploaded_by",
    )
