from rest_framework.routers import DefaultRouter

from django.urls import include, path

# pylint:disable=unused-import
from . import app_name  # noqa: F401
from .api_views import (
    ProjectViewSet,
    ProjectFileViewSet,
    ProjectFileByProjectView,
)

router = DefaultRouter()
router.register(r"projects", ProjectViewSet)
router.register(r"projectfiles", ProjectFileViewSet)

urlpatterns = [
    path(
        "projects/<uuid:uuid>/projectfiles/",
        ProjectFileByProjectView.as_view(),
    ),
    path("", include(router.urls)),
]
