import uuid
from markdownx.models import MarkdownxField

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Q
from django.utils import timezone

from intracdc.conf.storage_backends import PrivateMediaStorage

from accounts.models import Class, Semester, Account

from .utils import SafeDict


class ProjectQuerySet(models.QuerySet):
    def by_semester(self, semester: Semester):
        return self.filter(semester=semester)

    def by_class(self, classe: Class):
        return self.by_semester(classe.semester)

    def by_user(self, user: Account):
        if user.is_superuser:
            return self

        filter_query = Q(managers__username=user.username)

        if user.current_class is None and user.past_classes.count() == 0:
            return self.filter(filter_query)

        if user.current_class is not None:
            filter_query |= Q(semester=user.current_class.semester)
        for past_class in user.past_classes.all():
            filter_query |= Q(semester=past_class.semester)

        query = self.filter(filter_query)

        if user.is_student():
            query = query.published()

        return query

    def published(self):
        now = timezone.now()
        return self.filter(begin__gte=now)

    def unpublished(self):
        now = timezone.now()
        return self.filter(begin__lte=now)

    def ongoing(self):
        now = timezone.now()
        return self.filter(begin__gte=now, end__lte=now)


class Project(models.Model):
    class ProjectType(models.TextChoices):
        OCAML = "ML", "OCaml"
        CSHARP = "CS", "C Sharp"

    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )

    number = models.IntegerField()
    type = models.CharField(max_length=2, choices=ProjectType.choices)
    title = models.CharField(max_length=255)
    semester = models.ForeignKey("accounts.Semester", on_delete=models.PROTECT)
    managers = models.ManyToManyField(
        get_user_model(),
        limit_choices_to={"groups__name__in": ["ACDC", "Teachers", "CRI"]},
    )

    description = MarkdownxField(blank=True)

    begin = models.DateTimeField()
    end = models.DateTimeField()

    custom_git_url = models.CharField(max_length=255, blank=True)

    objects = ProjectQuerySet.as_manager()

    def compute_git_url(self, user):
        if not user.is_authenticated:
            return ""
        if len(self.custom_git_url) != 0:
            return self.custom_git_url.format_map(
                SafeDict(login=user.username)
            )
        return (
            f"git@git.cri.epita.fr:p/"
            f"{ self.semester.promotion }-s{ self.semester.number }"
            f"{ 's' if self.semester.is_sharp else '' }-tp/"
            f"{ 'caml'if self.type == 'ML' else 'csharp' }-"
            f"tp{ self.number }-{ user.username }"
        )

    def is_unpublished(self):
        return timezone.now() < self.begin

    def is_published(self):
        return self.begin <= timezone.now()

    def is_ongoing(self):
        now = timezone.now()
        return self.begin <= now() < self.end

    def is_finished(self):
        return self.end <= timezone.now()

    def __str__(self):
        return f"{self.semester} - {self.type} - {self.number} - {self.title}"


class ProjectFileQuerySet(models.QuerySet):
    def by_project(self, project: Project):
        return self.filter(project=project)

    def by_user(self, user: Account):
        return self.filter(project__in=Project.objects.by_user(user))


class ProjectFile(models.Model):
    class FileType(models.TextChoices):
        SUBJECT_FR = "FR", "French subject"
        SUBJECT_EN = "EN", "English subject"
        SKELETON = "SK", "Skeleton"
        CORRECTION = "CR", "Correction"
        OTHER = "OT", "Other"

    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    project = models.ForeignKey("Project", on_delete=models.CASCADE)
    file = models.FileField(storage=PrivateMediaStorage())

    type = models.CharField(max_length=2, choices=FileType.choices)
    type_other_description = models.CharField(max_length=255, blank=True)

    is_private = models.BooleanField(default=True)

    version_major = models.IntegerField()
    version_minor = models.IntegerField(default=0)
    version_patch = models.IntegerField(default=0)
    version_build = models.IntegerField(default=0)

    uploaded_by = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    objects = ProjectFileQuerySet.as_manager()

    def get_version(self):
        return f"{self.version_major}.{self.version_minor}.{self.version_patch}.{self.version_build}"  # noqa: E501

    # pylint:disable=signature-differs
    def delete(self, *args, **kwargs):
        self.file.delete(False)
        return super(ProjectFile, self).delete(*args, **kwargs)

    def __str__(self):
        return f"{self.file}"
