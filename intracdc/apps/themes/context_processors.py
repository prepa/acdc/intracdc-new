from .models import Theme, DatedTheme


def load_theme(request):
    if not request.user.is_authenticated:
        theme = Theme.objects.first()
    else:
        theme = request.user.theme

    for dated_theme in DatedTheme.objects.all():
        if dated_theme.is_current():
            theme = dated_theme.theme

    return {
        "theme": theme,
    }
