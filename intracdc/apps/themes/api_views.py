from rest_framework import permissions
from rest_framework import viewsets

from .models import Theme, DatedTheme, ThemeImage
from .serializers import (
    ThemeSerializer,
    DatedThemeSerializer,
    ThemeImageSerializer,
)


class ThemeViewSet(viewsets.ModelViewSet):
    queryset = Theme.objects.all()
    serializer_class = ThemeSerializer
    permission_classes = [permissions.DjangoModelPermissions]


class DatedThemeViewSet(viewsets.ModelViewSet):
    queryset = DatedTheme.objects.all()
    serializer_class = DatedThemeSerializer
    permission_classes = [permissions.DjangoModelPermissions]


class ThemeImageViewSet(viewsets.ModelViewSet):
    queryset = ThemeImage.objects.all()
    serializer_class = ThemeImageSerializer
    permission_classes = [permissions.DjangoModelPermissions]
