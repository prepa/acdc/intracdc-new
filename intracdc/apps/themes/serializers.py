from rest_framework import serializers

from .models import Theme, DatedTheme, ThemeImage
from . import app_name


class ThemeSerializer(serializers.HyperlinkedModelSerializer):
    favicon = serializers.HyperlinkedRelatedField(
        queryset=ThemeImage.objects.all(),
        view_name=f"{app_name}:themeimage-detail",
    )
    logo = serializers.HyperlinkedRelatedField(
        queryset=ThemeImage.objects.all(),
        view_name=f"{app_name}:themeimage-detail",
    )
    background = serializers.HyperlinkedRelatedField(
        queryset=ThemeImage.objects.all(),
        view_name=f"{app_name}:themeimage-detail",
    )

    falling_images = serializers.HyperlinkedRelatedField(
        many=True,
        queryset=ThemeImage.objects.all(),
        view_name=f"{app_name}:themeimage-detail",
    )

    class Meta:
        model = Theme
        fields = (
            "url",
            "id",
            "name",
            "favicon",
            "logo",
            "background",
            "falling_images",
            "style",
        )
        extra_kwargs = {"url": {"view_name": f"{app_name}:theme-detail"}}


class DatedThemeSerializer(serializers.HyperlinkedModelSerializer):
    theme = serializers.HyperlinkedRelatedField(
        queryset=Theme.objects.all(), view_name=f"{app_name}:theme-detail"
    )

    class Meta:
        model = DatedTheme
        fields = ("url", "theme", "start_date", "end_date")
        extra_kwargs = {"url": {"view_name": f"{app_name}:datedtheme-detail"}}


class ThemeImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ThemeImage
        fields = ("url", "id", "image")
        extra_kwargs = {"url": {"view_name": f"{app_name}:themeimage-detail"}}
