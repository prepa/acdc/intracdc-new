from django.db import models
from django.utils import timezone

from intracdc.conf.storage_backends import PublicMediaStorage


class Theme(models.Model):
    name = models.CharField(max_length=255, unique=True)

    favicon = models.ForeignKey(
        "ThemeImage", on_delete=models.DO_NOTHING, related_name="favicon"
    )
    logo = models.ForeignKey(
        "ThemeImage", on_delete=models.DO_NOTHING, related_name="logo"
    )
    background = models.ForeignKey(
        "ThemeImage", on_delete=models.DO_NOTHING, related_name="background"
    )

    falling_images = models.ManyToManyField(
        "ThemeImage", blank=True, related_name="falling_images"
    )

    style = models.FileField(storage=PublicMediaStorage())

    def __str__(self):
        return f"{self.name}"


class DatedTheme(models.Model):
    theme = models.OneToOneField(
        "Theme", on_delete=models.CASCADE, primary_key=True
    )
    start_date = models.DateField()
    end_date = models.DateField()

    def is_current(self):
        now = timezone.now().date()
        start_date = self.start_date.replace(now.year)
        end_date = self.end_date.replace(now.year)
        return start_date <= now <= end_date

    def __str__(self):
        return f"{self.theme.name}"


class ThemeImage(models.Model):
    image = models.ImageField(storage=PublicMediaStorage())

    def __str__(self):
        return f"{self.image}"
