from django.contrib import admin

from .models import Theme, DatedTheme, ThemeImage


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    pass


@admin.register(DatedTheme)
class DatedThemeAdmin(admin.ModelAdmin):
    pass


@admin.register(ThemeImage)
class ThemeImageAdmin(admin.ModelAdmin):
    pass
