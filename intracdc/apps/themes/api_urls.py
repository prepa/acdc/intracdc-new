from rest_framework.routers import DefaultRouter

from django.urls import include, path

# pylint:disable=unused-import
from . import app_name  # noqa: F401
from .api_views import (
    ThemeViewSet,
    DatedThemeViewSet,
    ThemeImageViewSet,
)

router = DefaultRouter()
router.register(r"themes", ThemeViewSet)
router.register(r"datedthemes", DatedThemeViewSet)
router.register(r"themeimage", ThemeImageViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
