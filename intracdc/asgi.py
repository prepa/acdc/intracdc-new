"""
ASGI config for scoreboard project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

from django.core.asgi import get_asgi_application
from intracdc.manage import setup

setup()

# pylint:disable=invalid-name
application = get_asgi_application()
