from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from django.conf import settings
from django.contrib import admin
from django.urls import include, path


# pylint: disable=redefined-builtin
@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "accounts": reverse(
                "api_accounts:api-root", request=request, format=format
            ),
            "projects": reverse(
                "api_projects:api-root", request=request, format=format
            ),
            "themes": reverse(
                "api_themes:api-root", request=request, format=format
            ),
        }
    )


urlpatterns = [
    path("admin/", admin.site.urls),
    path("markdownx/", include("markdownx.urls")),
    path("api/", api_root),
    path(
        "api/accounts/", include("accounts.api_urls", namespace="api_accounts")
    ),
    path(
        "api/projects/", include("projects.api_urls", namespace="api_projects")
    ),
    path("api/themes/", include("themes.api_urls", namespace="api_themes")),
    path(
        "api-auth/", include("rest_framework.urls", namespace="rest_framework")
    ),
    path("", include("home.urls", namespace="home")),
    path("accounts/", include("accounts.urls", namespace="accounts")),
    path("accounts/", include("social_django.urls", namespace="social")),
    path("projects/", include("projects.urls", namespace="projects")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
