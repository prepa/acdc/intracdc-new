"""
Django settings for intracdc project.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import os
import sys

try:
    sys.path.append("/etc/intracdc/")
    # pylint:disable=unused-wildcard-import
    from settings import *  # noqa: F401

    print("### Using settings module from /etc/intracdc/settings.py ###")
except ImportError:
    # pylint:disable=unused-wildcard-import
    from intracdc.conf.local_settings import *  # noqa: F401

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
APPS_DIR = os.path.join(BASE_DIR, "apps")
sys.path.append(APPS_DIR)

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.postgres",
    "debug_toolbar",
    "social_django",
    "rest_framework",
    "markdownx",
    "home.apps.HomeConfig",
    "accounts.apps.AccountsConfig",
    "themes.apps.ThemesConfig",
    "projects.apps.ProjectsConfig",
]

MIDDLEWARE = [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "social_django.middleware.SocialAuthExceptionMiddleware",
]

ROOT_URLCONF = "intracdc.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "social_django.context_processors.backends",
                "social_django.context_processors.login_redirect",
                "themes.context_processors.load_theme",
            ],
        },
    },
]

WSGI_APPLICATION = "intracdc.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

AUTHENTICATION_BACKENDS = (
    "epita_connect.backend.EpitaOpenIdConnect",
    "django.contrib.auth.backends.ModelBackend",
)

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

# pylint:disable=line-too-long
AUTH_PASSWORD_VALIDATORS = [
    {
        # pylint:disable=line-too-long
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"  # noqa: E501
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"
    },
]

# Logging
# https://docs.djangoproject.com/en/3.0/topics/logging/

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {"console": {"class": "logging.StreamHandler"}},
    "root": {"handlers": ["console"], "level": "INFO"},
}

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "Europe/Paris"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# User model

AUTH_USER_MODEL = "accounts.Account"

# Social Auth settings

LOGIN_URL = "/accounts/login/epita"
ENABLE_EPITA_CONNECT = True
SOCIAL_AUTH_URL_NAMESPACE = "social"
SOCIAL_AUTH_LOGIN_ERROR_URL = "/login-error"
SOCIAL_AUTH_LOGIN_REDIRECT_URL = "/"
SOCIAL_AUTH_EPITA_SCOPE = ["epita"]
SOCIAL_AUTH_EPITA_EXTRA_DATA = ["promo"]
SOCIAL_AUTH_EPITA_BETA = False

SOCIAL_AUTH_PIPELINE = (
    "social_core.pipeline.social_auth.social_details",
    "social_core.pipeline.social_auth.social_uid",
    "social_core.pipeline.social_auth.auth_allowed",
    "social_core.pipeline.social_auth.social_user",
    "social_core.pipeline.social_auth.associate_by_email",
    "social_core.pipeline.social_auth.associate_user",
    "social_core.pipeline.social_auth.load_extra_data",
    "social_core.pipeline.user.user_details",
)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

AWS_AUTO_CREATE_BUCKET = True
AWS_BUCKET_ACL = "public-read"
AWS_DEFAULT_ACL = "public-read"
AWS_QUERYSTRING_AUTH = True
# AWS_S3_OBJECT_PARAMETERS = {
#    "CacheControl": "max-age=86400",
# }

AWS_STATIC_LOCATION = "static"
STATIC_URL = f"/{AWS_STORAGE_BUCKET_NAME}/{AWS_STATIC_LOCATION}/"  # noqa: F999
STATICFILES_STORAGE = "intracdc.conf.storage_backends.StaticStorage"

AWS_PUBLIC_MEDIA_LOCATION = "media/public"
MEDIA_URL = (
    f"/{AWS_STORAGE_BUCKET_NAME}/{AWS_PUBLIC_MEDIA_LOCATION}/"  # noqa: F999
)
DEFAULT_FILE_STORAGE = "intracdc.conf.storage_backends.PublicMediaStorage"

AWS_PRIVATE_MEDIA_LOCATION = "media/private"
PRIVATE_FILE_STORAGE = "intracdc.conf.storage_backends.PrivateMediaStorage"


REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": 100,
}


MARKDOWNX_MARKDOWN_EXTENSIONS = [
    "markdown.extensions.extra",
    "markdown.extensions.codehilite",
]


# Django debug toolbar
def show_toolbar(request):
    from django.conf import settings  # pylint:disable=import-outside-toplevel

    return settings.DEBUG


DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": "intracdc.conf.settings.show_toolbar",
    "SHOW_COLLAPSED": True,
}
