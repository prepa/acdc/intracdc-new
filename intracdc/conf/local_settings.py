from . import env

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.get_secret("DJANGO_SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.get_bool("DJANGO_DEBUG", False)

ALLOWED_HOSTS = env.get_list("DJANGO_ALLOWED_HOSTS", [], separator=" ")

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env.get_string("DJANGO_DB_NAME", "intracdc"),
        "USER": env.get_string("DJANGO_DB_USER", "intracdc"),
        "PASSWORD": env.get_secret("DJANGO_DB_PASSWORD"),
        "HOST": env.get_string("DJANGO_DB_HOST"),
        "PORT": env.get_string("DJANGO_DB_PORT", "5432"),
    }
}

# Social Auth keys

SOCIAL_AUTH_EPITA_KEY = env.get_secret("DJANGO_SOCIAL_AUTH_EPITA_KEY")
SOCIAL_AUTH_EPITA_SECRET = env.get_secret("DJANGO_SOCIAL_AUTH_EPITA_SECRET")

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

AWS_ACCESS_KEY_ID = env.get_secret("S3_ACCESS_KEY")
AWS_SECRET_ACCESS_KEY = env.get_secret("S3_SECRET_KEY")
AWS_STORAGE_BUCKET_NAME = env.get_string("S3_BUCKET", "intracdc")
AWS_S3_ENDPOINT_URL = env.get_string("S3_ENDPOINT")
AWS_S3_CUSTOM_DOMAIN = env.get_string("S3_CUSTOM_DOMAIN", "") or None
AWS_S3_SECURE_URLS = env.get_bool("S3_SECURE_URLS", True)
