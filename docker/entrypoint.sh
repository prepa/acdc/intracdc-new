#!/bin/sh

mkdir -p $HOME/.ipython/profile_default/
cp docker/config/ipython_config.py $HOME/.ipython/profile_default/

if [ ! -z $1 ]; then
  exec ./manage.py "$@"
fi

./intracdc/manage.py collectstatic --noinput

if [ ! -z $MIGRATE ]; then
  ./intracdc/manage.py migrate
fi

if [ ! -z $DEV ]; then
  exec ./intracdc/manage.py runserver 0.0.0.0:8000
fi

exec gunicorn intracdc.conf.wsgi -b 0.0.0.0:8000
