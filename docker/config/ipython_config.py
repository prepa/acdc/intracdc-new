# Configuration file for ipython.

# pylint:disable=undefined-variable
c.InteractiveShellApp.extensions = ["autoreload"]  # noqa: F821
c.InteractiveShellApp.exec_lines = ["%autoreload 2"]  # noqa: F821
